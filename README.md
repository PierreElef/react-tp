Pour cloner le projet :

### `git clone https://gitlab.com/PierreElef/horloge-berlin.git`

Pour installer les packages :

### `cd react-tp`
### `npm install`

Pour lancer l'interface :

### `npm start`

Pour voir l'interface :

[http://localhost:3000/](http://localhost:3000/)

Pour voir la version en ligne :

[https://cranky-poincare-ff9137.netlify.app](https://cranky-poincare-ff9137.netlify.app)
