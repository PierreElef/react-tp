import React from 'react';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Main from './components/Main/Main';
import{ BrowserRouter as Router} from "react-router-dom";
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Header></Header>
        <Main></Main>
        <Footer></Footer>
      </Router>
    </div>
  );
}

export default App;
