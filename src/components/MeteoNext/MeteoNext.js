import React, { Component } from 'react';
import MeteoMiniCard from '../MeteoMiniCard/MeteoMiniCard';
import Loader from '../Loader/Loader';

export default class MeteoNext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      area : props.area,
      meteos:[]
    };
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.area !== prevProps.area) {
      this.setState(
        {area: this.props.area}
      );
      if(this.state.area){
        //FETCH Meteos
        var url = "https://api.openweathermap.org/data/2.5/forecast?lat="+this.state.area.lattitude+"&lon="+this.state.area.longitude+"&appid=2c6df9be89b28c2d81d7fc342f5b2731&units=metric"
        fetch(url)
        .then(
          res => res.json()
        )
        .then(
          (result) => {
              // console.log(result)
              this.setState(
                {meteos: result.list}
              );
          },
          (error) => {
              console.log(error)
          }
        )
      }
    }
  }

  render() {
    var meteos = this.state.meteos.slice(0, 4);
    if(this.state.meteos){
      return (
        <div className="meteonext row">
          <div className="col-12 text-center">
            <h2>Prévisions</h2>
          </div>
          <div className="col-12">
            <div className="row">
            { 
              meteos.map(meteo =>{
                return (
                  <div className='col-md-3 col-6 py-2' key={meteo.dt}>
                    <MeteoMiniCard meteo={meteo}/>
                  </div>
                )
              })
            }
            </div>
          </div>
        </div>
      )
    }else{
      return (
        <div className="meteonext text-center">
          <Loader />
        </div>
      )
    }
    
  }
}
