import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer } from 'recharts';
import Loader from '../Loader/Loader';

export default class Graph extends Component {

  constructor(props) {
    super(props);
    this.state = {
      area : props.area,
      data: null
    };
  }

  componentDidMount() {
    
  }

  
  componentDidUpdate(prevProps) {
    if (this.props.area !== prevProps.area) {
      this.setState(
        {area: this.props.area}
      );
      if(this.props.area){
        
        //FETCH Meteos
        var url = "https://api.openweathermap.org/data/2.5/forecast?q="+this.props.area.name+"&appid=2c6df9be89b28c2d81d7fc342f5b2731&units=metric"
        fetch(url)
        .then(
          res => res.json()
        )
        .then(
          (result) => {
            this.setState(
              {data : result.list}
            )
          },
          (error) => {
            console.log(error)
          }
        )
      }
    }
  }

  render() {
    if(this.state.data){
      const data= [];
      this.state.data.slice(0,8).forEach( dataToPush => {
          data.push(
            {
              "Heure": new Intl.DateTimeFormat('fr-FR', {hour: "numeric", minute: "numeric", hour12: false}).format(new Date(dataToPush.dt_txt)),
              "Temp": dataToPush.main.temp
            }
          )
        })
      return (
        <div className="graph text-center">
          <h2>Graphique des températures</h2>
          <ResponsiveContainer width="100%" height={250}>
            <LineChart data={data}
              margin={{ top: 5, right: 5, left: 5, bottom: 5 }}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="Heure" />
              <YAxis />
              <Tooltip />
              <Line type="monotone" dataKey="Temp" stroke="#8884d8" />
            </LineChart>
            </ResponsiveContainer>
        </div>
      )
    }else{
      return(
        <div className="graph text-center">
          <Loader />
        </div>
      )
    }
    
  }
}
