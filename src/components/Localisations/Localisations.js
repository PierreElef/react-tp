import React, { Component } from 'react';

import AddLocalisation from '../AddLocalisation/AddLocalisation';
import EditLocalisation  from '../EditLocalisation/EditLocalisation';

export default class Localisations extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name : 'Albi',
      lattitude : '0',
      longitude : '0',
      message: props.message,
      areas: props.areas,
      areaToDelete : null
    };
    this.addArea = this.addArea.bind(this);
    this.editArea = this.editArea.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeLattitude = this.handleChangeLattitude.bind(this);
    this.handleChangeLongitude = this.handleChangeLongitude.bind(this);
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (prevProps.message !== this.props.message) {
      this.setState(
        {message: this.props.message}
      );
    }
    if (prevProps.areas !== this.props.areas) {
      this.setState(
        {areas: this.props.areas}
      );
    }
  }

  handleChangeName(event){
    this.setState(
      {name:event.target.value}
    )
  }
  handleChangeLattitude(event){
    this.setState(
      {lattitude:event.target.value}
    )
  }
  handleChangeLongitude(event){
    this.setState(
      {longitude:event.target.value}
    )
  }

  addArea(area){
    this.props.addArea(area)
  }

  editArea(area){
    this.setState(
      {
        name: area.name,
        lattitude: area.lattitude,
        longitude: area.longitude,
      }
    )
  }

  render() {
    return (
      <div className="addlocalisation container">
        <div className="row">
          <div className="col-12 text-center pb-2">
            <h1>Gestion des localisations</h1>
          </div>
          <div className="col-md-6 col-12">
            <AddLocalisation 
              name={this.state.name}
              lattitude = {this.state.lattitude}
              longitude = {this.state.longitude}
              message = {this.state.message}
              addArea = {this.addArea}
              handleChangeName = {this.handleChangeName}
              handleChangeLattitude = {this.handleChangeLattitude}
              handleChangeLongitude = {this.handleChangeLongitude}
            />
          </div>
          <div className="col-md-6 col-12">
            <EditLocalisation 
              message = {this.state.message}
              areas = {this.state.areas}
              editArea = {this.editArea}
              deleteArea = {this.props.deleteArea}
            />
          </div>
        </div>
      </div>
    )
  }
}