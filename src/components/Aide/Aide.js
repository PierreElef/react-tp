import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt, faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons';

export default class Aide extends Component {
  render() {
    return (
      <div className="aide container">
        <div className="row">
          <div className="col-12 text-center pb-3">
            <h1>Guide d'utilisation</h1>
          </div>
          <div className="col-12">
            <h2>Gestion de localisation</h2>
            <h3>Ajouter une localisation</h3>
            <ul>
              <li>Ecrirez le nom d’une ville.</li>
              <li>Notez les coordonnées de la ville.</li>
              <li>Vous pouvez générer les coordonées en cliquant sur <button className="btn btn-save" type="button"><FontAwesomeIcon icon={faMapMarkerAlt} /></button></li>
              <li>Cliquer sur le bouton <input className="btn btn-save" type="submit" value="Enregistrer"/></li>
            </ul> 
            <h3>Editer une localisation</h3>
            <ul>
              <li>Cliquez sur <button className="btn-edit mx-2 my-1" type="button"><FontAwesomeIcon icon={faPencilAlt} /></button></li>
              <li>Changer les coordonnées de la ville.</li>
              <li>Vous pouvez générer les coordonées en cliquant sur <button className="btn btn-save" type="button"><FontAwesomeIcon icon={faMapMarkerAlt} /> </button></li>
              <li>Cliquer sur le bouton <input className="btn btn-save" type="submit" value="Enregistrer"/></li>
            </ul> 
            <h3>Supprimer une localisation</h3>
            <ul>
              <li>Cliquez sur <button className="btn-delete mx-2 my-1" type="button"><FontAwesomeIcon icon={faTrash} /></button></li>
            </ul> 
          </div>
        </div>
      </div>
    )
  }
}
