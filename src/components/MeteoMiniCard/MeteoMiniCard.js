import React, { Component } from 'react';

export default class MeteoMiniCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      meteo : props.meteo
    };
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.meteo !== prevProps.meteo) {
      this.setState(
        {meteo: this.props.meteo}
      );
    }
  }

  render() {
    var date = new Date(this.state.meteo.dt_txt);
    return (
      <div className="meteominicard row">
        <div className="col-12">
          {new Intl.DateTimeFormat('fr-FR', {month: "numeric", day: "numeric"}).format(date)} - {new Intl.DateTimeFormat('fr-FR', {hour: "numeric", minute: "numeric", hour12: false}).format(date)}
        </div>
        <div className="col-12">
          <img className="iconCard border border-black" src={"https://openweathermap.org/img/wn/"+ this.state.meteo.weather[0].icon+"@2x.png"} alt={ this.state.meteo.weather[0].description}/>
        </div>
        <div className="col-12">
          <h4>{this.state.meteo.main.temp}°C</h4>
          <small>{this.state.meteo.main.feels_like}°C ressenti</small>
        </div>
      </div>
    )
  }
}
