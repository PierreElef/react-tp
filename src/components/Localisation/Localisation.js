import React, { Component } from 'react';

export default class Localisation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      areas : props.areas
    };
    this.chooseArea = props.chooseArea
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.areas !== prevProps.areas) {
      this.setState(
        {areas: this.props.areas}
      );
    }
  }

  chooseArea(event){
    return(event.target.value);
  }


  render() {
    return (
      <div className="localisation form-group">
        <label>Choisissez votre localisation :</label><br/>
        <select name='area' className="form-control" onChange={this.chooseArea} >
          { this.state.areas.map(area => {
            return (
              <option value={area.name} key={area.name}>{area.name}</option>
            )
          })}
        </select>
      </div>
    )
  }
}
