import React, { Component } from 'react';
export default class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      year : new Date().getFullYear()
    };
  }

  render() {
    return (
      <footer className="footer fixed-bottom text-center py-3">
            © {this.state.year} Pierre Elefterion
      </footer>
    )
  }
}
