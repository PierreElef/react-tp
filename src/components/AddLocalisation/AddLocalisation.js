import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

export default class AddLocalisation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name : props.name,
      lattitude : props.lattitude,
      longitude : props.longitude,
      message: props.message,
    };
    this.addArea = this.addArea.bind(this);
    this.handleChangeName = props.handleChangeName;
    this.handleChangeLattitude = props.handleChangeLattitude;
    this.handleChangeLongitude = props.handleChangeLongitude;
    this.findCoord = this.findCoord.bind(this);
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (prevProps.name !== this.props.name) {
      this.setState(
        {name: this.props.name}
      );
    }
    if (prevProps.lattitude !== this.props.lattitude) {
      this.setState(
        {lattitude: this.props.lattitude}
      );
    }
    if (prevProps.longitude !== this.props.longitude) {
      this.setState(
        {longitude: this.props.longitude}
      );
    }
    if (prevProps.message !== this.props.message) {
      this.setState(
        {message: this.props.message}
      );
    }
    if (prevProps.areas !== this.props.areas) {
      this.setState(
        {areas: this.props.areas}
      );
    }
  }

  handleChangeName(event){
    this.setState(
      {name:event.target.value}
    )
    return event;
  }
  handleChangeLattitude(event){
    this.setState(
      {lattitude:event.target.value}
    )
    return event;
  }
  handleChangeLongitude(event){
    this.setState(
      {longitude:event.target.value}
    )
    return event;
  }

  findCoord(event){
    event.preventDefault();
    if(this.state.name){
      var url = "https://api-adresse.data.gouv.fr/search/?q="+this.state.name;
      fetch(url)
      .then(
        res => res.json()
      )
      .then(
          (result) => {
            this.setState(
              {
                longitude:result.features[0].geometry.coordinates[0],
                lattitude:result.features[0].geometry.coordinates[1]
              }
            );
          },
          (error) => {
              console.log(error)
          }
      )
    }
  }

  addArea(event){
    event.preventDefault();
    var area = {
      name: this.state.name,
      lattitude: this.state.lattitude,
      longitude: this.state.longitude,
    }
    this.props.addArea(area)
  }

  render() {
    if(this.state.message === 'success'){
      var message = <div className="alert alert-success" role="alert">Cette localisation a été ajoutée.</div>
    }else if(this.state.message === 'info'){
      message = <div className="alert alert-info" role="alert">Cette localisation a été changée.</div>
    }else{
      message = '';
    }
    return (
      <div className="addlocalisation">
        <h2 className="py-1">Ajouter</h2>
            <form className="row" >
              <div className="form-group col-12">
                <label>Nom</label>
                <div className="input-group mb-3">
                  <input type='text' className="form-control" value={this.state.name} onChange={this.handleChangeName} aria-describedby="button-addon2"></input>
                  <div className="input-group-append">
                      <button className="btn btn-save" type="button" id="button-addon2" onClick={this.findCoord}>
                        <FontAwesomeIcon icon={faMapMarkerAlt} />
                      </button>
                  </div>
                </div>
              </div>
              <div className="form-group col-md-6 col-12">
                <label>Lattitude</label>
                <input type='text' className="form-control" value={this.state.lattitude} onChange={this.handleChangeLattitude}></input>
              </div>
              <div className="form-group col-md-6 col-12">
                <label>Longitude</label>
                <input type='text' className="form-control" value={this.state.longitude} onChange={this.handleChangeLongitude}></input>
              </div>
              <div className="form-group col-12">
                <input className="btn btn-save" type="submit" value="Enregistrer" onClick={this.addArea}/>
              </div>
              <div className="col-12">
                {message}
              </div>
            </form>
      </div>
    )
  }
}
