import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons';

export default class EditLocalisation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      message: props.message,
      areas: props.areas,
      areaToDelete : null
    };
  }

  render() {
    if(this.state.message === 'danger'){
      var messageDelete = <div className="alert alert-danger" role="alert">Vous devez laissez au moins une localisation</div>
    }else{
      messageDelete = ''
    }
    return (
      <div className="editlocalisation">
        <h2 className="py-1">Gestion</h2>
            {messageDelete}
            <div className="row font-weight-bold mx-2 bg-light">
              <div className="col-8 border py-2">Nom</div>
              <div className="col-4 text-center border py-2">Gestion</div>
            </div>
            {
              this.state.areas.map(area => {
                return(
                  <div className="row mx-2 bg-light" key={area.name}>
                    <div className="col-8 border py-1">
                      {area.name}
                    </div>
                    <div className="col-4 text-center border">
                      <button className="btn-edit mx-2 my-1" onClick={() => this.props.editArea(area)}>
                        <FontAwesomeIcon icon={faPencilAlt} />
                      </button>
                      <button className="btn-delete mx-2 my-1" onClick={() => this.props.deleteArea(area.name)} value={area.name}>
                        <FontAwesomeIcon icon={faTrash} />
                      </button>
                    </div>
                  </div>
                )
              })
            }
      </div>
    )
  }
}
