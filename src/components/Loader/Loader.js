import React, { Component } from 'react';

export default class Loader extends Component {
  render() {
    return (
      <div className="loader">
        <img src={process.env.PUBLIC_URL + '/assets/img/loader.gif'} alt='lodaer'/>
      </div>
    )
  }
}
