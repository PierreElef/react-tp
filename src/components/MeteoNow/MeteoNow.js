import React, { Component } from 'react';
import MeteoCard from '../MeteoCard/MeteoCard';
import Loader from '../Loader/Loader';

export default class MeteoNow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      area : props.area,
      meteo:null
    };
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.area !== prevProps.area) {
      this.setState(
        {area: this.props.area}
      );
      if(this.state.area){
        //FETCH Meteo Now
        var url = "https://api.openweathermap.org/data/2.5/weather?lat="+this.state.area.lattitude+"&lon="+this.state.area.longitude+"&appid=2c6df9be89b28c2d81d7fc342f5b2731&units=metric"
        fetch(url)
        .then(
          res => res.json()
        )
        .then(
          (result) => {
              // console.log(result)
              this.setState(
                {meteo: result}
              );
          },
          (error) => {
              console.log(error)
          }
        )
      }
    }
  }

  render() {
    if(this.state.meteo){
      return (
        <div className="meteonow">
          <MeteoCard meteo={this.state.meteo} />
        </div>
      )
    }else{
      return (
        <div className="meteonow">
          <Loader />
        </div>
      )
    }
    
  }
}
