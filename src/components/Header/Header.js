import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSun } from '@fortawesome/free-solid-svg-icons';

import { Link } from "react-router-dom";

export default class Header extends Component {

  render() {
    var links = [
      {
        name : "Accueil",
        path: "/"
      },
      {
        name : "Graphique",
        path: "/graph"
      },
      {
        name : "Carte",
        path: "/map"
      },
      {
        name : "Gestion",
        path: "/management"
      },
      {
        name : "Aide",
        path: "/aide"
      }
    ];
    
    return (
      <header className="header">
        <div className="container">
          <div className='row  text-center'>
            <div className='col-12'>
              <h1>
                <FontAwesomeIcon icon={faSun} className="yellow" /> Ma météo <FontAwesomeIcon icon={faSun} />
              </h1>
            </div>
            <div className='col-12 my-2'>
              <nav className="nav justify-content-center">
                {
                  links.map(link => {
                      var classLink = "nav-link btn mx-2";
                    return(
                      <Link key={link.name} className={classLink} to={link.path}>{link.name}</Link>
                    );
                  })
                }
              </nav>
            </div>
          </div>
        </div>
      </header>
    )
  }
}
