import React, { Component } from 'react';
import Localisation from '../Localisation/Localisation';
import MeteoNext from '../MeteoNext/MeteoNext';
import MeteoNow from '../MeteoNow/MeteoNow';

export default class Homepage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      areas : props.areas,
      area : null
    };
    this.chooseArea = this.chooseArea.bind(this);
  }

  componentDidMount() {
    this.setState(
      {area: this.state.areas[0]}
    );
  }
  
  componentDidUpdate(prevProps) {
    if (prevProps.newArea !== this.state.area) {
      this.setState(
        {area: this.state.newArea}
      );
    }
    if (prevProps.areas !== this.props.areas) {
      this.setState(
        {areas: this.props.areas}
      );
    }
  }

  chooseArea(event){
    event.preventDefault();
    var newArea = this.state.areas.filter(area => {
      return area.name === event.target.value;         
    })
    this.setState({
      area : newArea[0]
    })
  }


  render() {
    return (
      <div className="homepage container">
        <div className="row">
          <div className="col-md-6 col-12 text-center">
            <Localisation areas = {this.state.areas} chooseArea={this.chooseArea} />
          </div>
          <div className="col-md-6 col-12 text-center mt-md-0 mt-3">
            <MeteoNow area = {this.state.area} />
          </div>
          <div className="col-12 text-center mt-3">
            <MeteoNext area = {this.state.area} />
          </div>
        </div>
      </div>
    )
  }
}
