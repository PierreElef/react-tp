import React, { Component } from 'react';
import Homepage from '../Homepage/Homepage';
import Localisations from '../Localisations/Localisations';
import GraphView from '../GraphView/GraphView';
import Aide from '../Aide/Aide';
import Map from '../Map/Map';
import{Switch, Route} from "react-router-dom";

export default class Main extends Component {

  constructor(props) {
    super(props);
    this.state = {
      areas : [
        {
          name: 'Toulouse',
          lattitude :43.6044622,
          longitude :1.4442469,
        },
        {
          name: 'Paris',
          lattitude :48.8534,
          longitude :2.3488,
        },
        {
          name: 'Bordeaux',
          lattitude :44.841225,
          longitude :-0.5800364,
        },
        {
          name: 'Strasbourg',
          lattitude :48.579831,
          longitude :7.761454,
        },
        {
          name: 'Brest',
          lattitude :48.406435,
          longitude :-4.497736,
        },
        {
          name: 'Lyon',
          lattitude :45.758,
          longitude :4.835,
        },
        {
          name: 'Nantes',
          lattitude :47.239367,
          longitude :-1.555335,
        },
        {
          name: 'Lille',
          lattitude :50.629026,
          longitude :3.061199,
        },
        {
          name: 'Nice',
          lattitude :43.712854,
          longitude :7.253866,
        },
        {
          name: 'Marseille',
          lattitude :43.282,
          longitude :5.405,
        },
        {
          name: 'Bourges',
          lattitude :47.081012,
          longitude :2.398782,
        },
      ],
      message:''
    };
    this.addArea = this.addArea.bind(this);
    this.deleteArea = this.deleteArea.bind(this);
  }

  
  componentDidMount() {
  }
  
  componentDidUpdate() {
  }

  componentWillUnmount() {
  }

  addArea(newArea){
    var alreadyExist;
    this.state.areas.forEach(area => {
      if(area.name === newArea.name){
        alreadyExist = true;
        area.name = newArea.name;
        area.lattitude = newArea.lattitude;
        area.longitude = newArea.longitude;
      }
    })
    if(!alreadyExist){
      this.state.areas.push(newArea)
      this.setState(
        {message:'success'}
      )
    }else{
      this.setState(
        {message:'info'}
      ) 
    }
  }

  deleteArea(areaToDelete){
    var nArea=-1;
    var find = false;
    this.state.areas.forEach(area => {
      if(area.name === areaToDelete){
        find = true;
        nArea += 1;
      }
      if(!find){
        nArea += 1;
      }
    })
    if(nArea !== -1 && this.state.areas.length !== 1){
      this.state.areas.splice((nArea), 1)
      var newAreas = this.state.areas;
      this.setState({ areas : newAreas})
    }else{
      this.setState(
        {message:'danger'}
      )
    }
  }

  render() {
    return (
      <main className="main">
        <Switch>
          <Route exact path='/'>
            <Homepage areas={this.state.areas} />
          </Route>
          <Route exact path='/management'>
            <Localisations message={this.state.message} areas={this.state.areas} addArea={this.addArea} deleteArea={this.deleteArea} />
          </Route>
          <Route exact path='/graph'>
            <GraphView areas={this.state.areas} />
          </Route>
          <Route exact path='/aide'>
            <Aide />
          </Route>
          <Route exact path='/map'>
            <Map areas={this.state.areas} />
          </Route>
        </Switch>
      </main>
    )
  }
}
