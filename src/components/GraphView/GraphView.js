import React, { Component } from 'react';
import Localisation from '../Localisation/Localisation';
import Graph from '../Graph/Graph';

export default class GraphView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      areas : props.areas,
      area : null
    };
    this.chooseArea = this.chooseArea.bind(this);
  }

  componentDidMount() {
    this.setState(
      {area: this.state.areas[0]}
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.areas !== this.props.areas) {
      this.setState(
        {areas: this.props.areas}
      );
    }
  }

  chooseArea(event){
    event.preventDefault();
    var newArea = this.state.areas.filter(area => {
      return area.name === event.target.value;         
    })
    this.setState({
      area : newArea[0]
    })
  }

  render() {
    return (
      <div className="graphview container">
        <div className="row">
          <div className="col-md-4 col-12">
            <Localisation areas = {this.state.areas} chooseArea={this.chooseArea} />
          </div>
          <div className="col-md-8 col-12">
            <Graph area = {this.state.area} />
          </div>
        </div>
      </div>
    )
  }
}
