import React, { Component } from 'react';

export default class MeteoCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      meteo : props.meteo
    };
  }

  componentDidMount() {
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.meteo !== prevProps.meteo) {
      this.setState(
        {meteo: this.props.meteo}
      );
    }
  }

  render() {
    
    var direction;
    if(this.state.meteo.wind.deg >= 22.5 && this.state.meteo.wind.deg < 67.5){
      direction = "Nord-Est";
    }else if(this.state.meteo.wind.deg >= 67.5 && this.state.meteo.wind.deg < 112.5){
      direction = "Est";
    }else if(this.state.meteo.wind.deg >= 112.5 && this.state.meteo.wind.deg < 157.5){
      direction = "Sud-Est";
    }else if(this.state.meteo.wind.deg >= 157.5 && this.state.meteo.wind.deg < 202.5){
      direction = "Sud";
    }else if(this.state.meteo.wind.deg >= 202.5 && this.state.meteo.wind.deg < 247.5){
      direction = "Sud-Ouest";
    }else if(this.state.meteo.wind.deg >= 247.5 && this.state.meteo.wind.deg < 292.5){
      direction = "Ouest";
    }else if(this.state.meteo.wind.deg >= 292.5 && this.state.meteo.wind.deg < 337.5){
      direction = "Nord-Ouest";
    }else{
      direction = "Nord";
    }

    return (
      <div className="meteocard">
        <div className="row">
          <div className="col-12">
            <h1 className="pb-3">{this.state.meteo.name}</h1>
          </div>
          <div className="col-md-6 col-12">
            <img className="iconCard border border-black" src={"https://openweathermap.org/img/wn/"+ this.state.meteo.weather[0].icon+"@2x.png"} alt={ this.state.meteo.weather[0].description}/>
          </div>
          <div className="col-md-6 col-12">
            <h2>{this.state.meteo.main.temp}°C</h2>
            <small>{this.state.meteo.main.feels_like}°C ressenti</small>
          </div>
          <div className="col-12">
            <div className="row text-center">
                <div className="col-6">Humidité : {this.state.meteo.main.humidity} %</div>
                <div className="col-6">Pression : {this.state.meteo.main.pressure} mbar</div>
                <div className="col-6">Vent vitesse : {this.state.meteo.wind.speed} km/h</div>
                <div className="col-6">Vent direction : {direction}</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
