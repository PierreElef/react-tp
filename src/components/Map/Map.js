import React, { Component } from 'react';

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      areas : props.areas,
      lat0: 51.08635768496341,
      lat100 : 42.34344220579775,
      lon0:-4.769477706422287,
      lon100 : 8.216662515296091,
      meteos:[]
    };
  }

  componentDidMount() {
    if(this.state.areas){
      var dataMeteos = [];
      this.props.areas.sort((a, b)=> a.lattitude - b.lattitude);
      console.log( this.props.areas)
      this.props.areas.forEach(area => {
         //FETCH Meteo Now
        var url = "https://api.openweathermap.org/data/2.5/weather?lat="+area.lattitude+"&lon="+area.longitude+"&appid=2c6df9be89b28c2d81d7fc342f5b2731&units=metric";
        fetch(url)
        .then(
          res => res.json()
        )
        .then(
          (result) => {
            dataMeteos.push(
                {
                  name:result.name,
                  lat : area.lattitude,
                  lon : area.longitude,
                  img: result.weather[0].icon+"@2x.png",
                  temp: result.main.temp,
                  wind: result.wind.speed
                }
              )
            this.setState({meteos : dataMeteos})
          },
          (error) => {
              console.log(error)
          }
        )
      })
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.areas !== this.props.areas) {
      this.setState(
        {areas: this.props.areas}
      );
    }
    
  }

  render() {
    if(this.state.meteos){
        return (
        <div className="map container">
          <div className="row justify-content-center">
            <div className="col-12 text-center">
              <h1>Carte</h1>
            </div>
            <div className="col-md-6 col-12 text-center map p-0">
              <img src={process.env.PUBLIC_URL + '/assets/img/carte4.png'} alt="map" className="w-100 mapImg" id="map"/>
              <div className="w-100 h-100">
                {
                  this.state.meteos.map((meteo, key) => {
                    var leftM = 100*(this.state.lon0 - meteo.lon)/(this.state.lon0-this.state.lon100);
                    var topM = 100*(this.state.lat0 - meteo.lat)/(this.state.lat0-this.state.lat100);
                    var imgStyle = {top:"calc("+topM+"% - 50px)", left:"calc("+leftM+"% - 50px)"} 
                    return(
                      <div className="meteoImg" style={imgStyle} key={key}>
                        <img src = {"https://openweathermap.org/img/wn/"+ meteo.img} alt={meteo.name} key={meteo.name}  />
                        <div className="margintop">{meteo.temp} °C<br/>{meteo.wind} km/h</div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </div>
        </div>
      )
    }else{
      return(
        <div className="map container"></div>
      )
    }
  }
}
